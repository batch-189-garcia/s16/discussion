let firstNumber = 12;
let secondNumber = 5;
let total = 0;

// ARITHMETIC OPERATORS
// Addition Operator - responsible for adding two or more numbers
total = firstNumber + secondNumber;
console.log('Result of addition operator is '+ total);

// Subtraction Operator - responsible for subtracting two or more numbers
total = firstNumber - secondNumber;
console.log('Result of subtraction operator is '+ total);

// Multiplication Operator - responsible for multiplying two or more numbers
total = firstNumber * secondNumber;
console.log('Result of multiplication operator is '+ total);

// Division Operator - responsible for dividing two or more numbers
total = firstNumber / secondNumber;
console.log('Result of division operator is '+ total);

// Modulo Operator - responsible for getting the remainder of two or more numbers
total = firstNumber % secondNumber;
console.log('Result of division operator is '+ total);

// ASSIGNMENTS OPERATORS
// Reassignment Operator - should be a single equals sign, signifies re-assignment or new value into existing variable
total = 27;
console.log('Result of Reassignment Operator operator is '+ total);

// Addition Assignment Operator - uses the current value of the variable and add a number to itself. Afterwards, reassigns it as the new value
total += 3;
// total = total + 3;
console.log('Result of Addition Assignment Operator operator is '+ total);

// Subtraction Assignment Operator - uses the current value of the variable and add a number to itself. Afterwards, reassigns it as the new value
total -= 5;
// total = total - 5;
console.log('Result of Subtraction Assignment Operator operator is '+ total);

// Multiplication Assignment Operator - uses the current value of the variable and add a number to itself. Afterwards, reassigns it as the new value
total *= 4;
// total = total * 4;
console.log('Result of Multiplication Assignment Operator operator is '+ total);

// Division Assignment Operator - uses the current value of the variable and add a number to itself. Afterwards, reassigns it as the new value
total /= 20;
// total = total / 20;
console.log('Result of Division Assignment Operator operator is '+ total);

// MULTIPLE OPERATORS
let mdasTotal = 0;
let pemdasTotal = 0;

/* 
When doing multiple operations, then program follows the MDAS rule. MDAS (Multiplication then Division then Addition then Substraction)
*/
mdasTotal = 2 + 1 - 5 * 4 / 1;
console.log(mdasTotal);

/* 
When doing multiple operations, then program follows the PEMDAS rule. PEMDAS (Parenthesis then Exponent then Multiplication then Division then Addition then Substraction)

Exponents are declared by using double asterisks ('**') after the number
*/

pemdasTotal = 5**2 + ( 10 - 2) / 2 * 3;
console.log(pemdasTotal);


// INCREMENT AND DECREMENT OPERATORS
let incrementNumber = 1;
let decrementNumber = 5;

/* 
    Pre-Increment - adds 1 first before reading value
    Pre-Decrement - substact 1 first before reading value

    Post-Increment - reads value first before adding 1
    Post-Decrement - reads value first before subtracting 1
*/ 
let resultOfPreIncrement = ++incrementNumber
let resultOfPreDecrement = --decrementNumber
let resultOfPostIncrement = incrementNumber++
let resultOfPostDecrement = decrementNumber--

console.log(resultOfPreIncrement);
console.log(resultOfPreDecrement);
console.log(resultOfPostIncrement);
console.log(resultOfPostDecrement);


// COERCION
// Coercion - when you add 2 or more non-string values
let a = '10';
let b = 10;

console.log(a + b);

// Non-Coercion - when you add 2 or more non-string values
let d = 10;
let f = 10;

console.log(d + f);


// Typeof Keyword - returns the data type of a variable
let stringType = 'Hello';
let numberType = 1;
let BooleanType = true;
let arrayType = ['1', '2', '3'];
let objectType = {
    objectKey: 'object value'
};

console.log(typeof stringType);
console.log(typeof numberType);
console.log(typeof booleanType);
console.log(typeof arrayType);
console.log(typeof objectType);

// Computer reads 'true' as 1
// Computer reads 'false' as 0
console.log(true + 1);
console.log(false + 1);

// COMPARISON OPERATORS
// Equality Operator - checks if both values are the same, returns true if it is
console.log(5 == 5);
console.log('hello' == 'hello');
console.log(2 == '2');

// Strict Equality Operator - checks if both values AND data types are the same, returns true if it is
console.log(2 === '2');
console.log(true === 1);

// Inequality Operator - checks if both values are not equal, returns true if they aren't
console.log(5 != 5);
console.log('hello' != 'hello');
console.log(2 != '2');

// Strict Inquality Oprator - checks if both values AND data types are NOT equal, return true if they aren't
console.log(1 !== '1');


// Relational Operators
let firstVariable = 5;
let secondVariable = 5;

// Greater than / GT Operator checks if first value is greater than the second
console.log(firstVariable > secondVariable);
// Less than / LT Operator checks if first value is less than the second
console.log(firstVariable < secondVariable);

// Greater than or equal / GTE Operator checks if first value is either greater than OR equal to the second
console.log(firstVariable >= secondVariable);
// Less  than or equal / LTE Operator checks if first value is either less than OR equal to the second
console.log(firstVariable <= secondVariable);


// LOGICAL OPERATORS
let isLegalAge = true;
let isRegistered = false;

// AND Operator - Returns true if both statements are true. Returns false if one of them is not true or Returns false if one of them is false.
console.log(isLegalAge && isRegistered);

// OR Operator - Returns true if both statements are true. Returns false if NONE of them is not true.or Returns true if one of them is true.
console.log(isLegalAge || isRegistered);

// NOT Operator - Reverses the boolean value (from true to false vice-versa)
console.log(!isLegalAge);

// Test
console.log(!isLegalAge || isRegistered);

// Truthy and Falsy values 
// Everything that is either emplty, zero or null will equate to false, and everything that has some sort of value will equate to true

console.log([] == false);
console.log('' == false);
// console.log(null == false);
console.log(1 == false);

/* 

The following values are always falsy:

false
0 (zero)
-0 (minus zero)
0n (BigInt zero)
'', "", `` (empty string)
null
undefined
NaN

Everything else is truthy. That includes:

'0' (a string containing a single zero)
'false' (a string containing the text “false”)
[] (an empty array)
{} (an empty object)
function(){} (an “empty” function)


*/ 